from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

from decimal import Decimal
import re

import requests

class GFLClanRuWebDemoReader:

    URL = "https://demos.gflclan.ru"
    ACCEPTED_GAMETYPES = ["csgo-ze", "css-mg", "css-surf", "css-ze"]
    DEFAULT_GAMETYPE = "css-ze"

    DEMO_ENTRY_REGEX = re.compile(
        "<tr>\\s*"
        "<td class=\"link\">\\s*"
        "<a href=\"(.*?)\".*?</a>\\s*"
        "</td>\\s*"
        "<td class=\"size\">\\s*"
        "(\\d+\\.\\d+)\\s*(.+)\\s*"
        "</td>\\s*"
        "<td class=\"date\">\\s*"
        "(.+)"
        "</td>\\s*"
        "</tr>"
        ,
        flags=re.MULTILINE | re.IGNORECASE
    )

    def __init__(self):
        raise Exception("Cannot instantiate this singleton!")

    @staticmethod
    def create_reader():
        game_type = ""
        while game_type not in GFLClanRuWebDemoReader.ACCEPTED_GAMETYPES:
            print("Enter the game type ({}; default {}): ".format(
                ", ".join(GFLClanRuWebDemoReader.ACCEPTED_GAMETYPES),
                GFLClanRuWebDemoReader.DEFAULT_GAMETYPE
            ))
            game_type = input()

            if not game_type:
                game_type = GFLClanRuWebDemoReader.DEFAULT_GAMETYPE

        demo_reader = GFLClanRuWebDemoReader.__InnerGFLClanRuWebDemoReader(
            game_type
        )

        return demo_reader

    class __InnerGFLClanRuWebDemoReader:

        def __init__(self, game_type):
            self._game_type = game_type

        def generate_statistics(self):

            raw_webpage = self.__download_webpage()
            processed_webpage = self.__process_webpage(raw_webpage)

            total_files_size = 0

            for demo_file_name in processed_webpage:
                total_files_size += processed_webpage[demo_file_name]["file_size"]

            statistics = {
                "total_files_size":  total_files_size,
                "files_count": len(processed_webpage)
            }

            return statistics

        def get_list_of_demos(self):

            raw_webpage = self.__download_webpage()
            processed_webpage = self.__process_webpage(raw_webpage)
            demos = [demo_file_name for demo_file_name in processed_webpage]

            return demos

        def get_base_url(self):
            return "{}/{}/".format(
                GFLClanRuWebDemoReader.URL,
                self._game_type
            )

        def __process_webpage(self, raw_webpage):

            processed_webpage = {}

            for match in GFLClanRuWebDemoReader.DEMO_ENTRY_REGEX.finditer(raw_webpage):
                file_size = Decimal(match.group(2))
                file_size_scale = match.group(3)

                # Scale the file size to Mebibytes.
                if file_size_scale == "MiB":
                    file_size *= Decimal(pow(1024, 0))
                elif file_size_scale == "KiB":
                    file_size *= Decimal(pow(1024, -1))

                processed_webpage[match.group(1)] = {
                    "file_size": file_size,
                    "date": match.group(4)
                }

            return processed_webpage

        def __download_webpage(self):

            request = requests.get(self.get_base_url())
            return request.text
