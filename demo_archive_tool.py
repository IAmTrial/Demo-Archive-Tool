from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

from enum import Enum

import requests

from webdemoreader.gflclan_ru_web_demo_reader import GFLClanRuWebDemoReader

ARCHIVE_ORG_SAVE_WEB_URL = "https://web.archive.org/save"

class Action(Enum):
    GENERATE_STATISTICS = 1
    DOWNLOAD_DEMOS = 2
    ARCHIVE_DEMOS = 3

description_by_action = {
    Action.GENERATE_STATISTICS: "Generate statistics on a range of demos.",
    Action.DOWNLOAD_DEMOS: "Download a range of demos.",
    Action.ARCHIVE_DEMOS: "Archive a range of demos using Archive.org's "
        "Wayback Machine."
}

def request_user_action():
    action = None
    while action is None:
        print("Choose an action:")
        for action in Action:
            print("{}) {}".format(action.value, description_by_action[action]))

        action_text = input()
        try:
            action = Action(int(action_text))
        except ValueError:
            pass

    return action

if __name__ == "__main__":
    reader_type = GFLClanRuWebDemoReader
    reader = reader_type.create_reader()
    action = request_user_action()

    if action == Action.GENERATE_STATISTICS:
        statistics = reader.generate_statistics()
        print("Total file count: {}".format(statistics["files_count"]))
        print("Total files size: {:.2f} {}".format(
            statistics["total_files_size"],
            "MiB"
        ))

    elif action == Action.DOWNLOAD_DEMOS:
        demos = reader.get_list_of_demos()

        for demo in demos:
            print("Downloading {}...".format(demo))
            final_url = "{}/{}".format(reader.get_base_url, demo)
            response = requests.get(final_url)
            with open(demo, "wb") as output_demo_file:
                output_demo_file.write(response.content)

    elif action == Action.ARCHIVE_DEMOS:
        demos = reader.get_list_of_demos()

        for demo in demos:
            print("Archiving {}...".format(demo))
            final_url = "{}/{}/{}".format(
                ARCHIVE_ORG_SAVE_WEB_URL,
                reader.get_base_url,
                demo
            )
            requests.get(final_url)

    print("Job complete!")
